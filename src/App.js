/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from "@emotion/react/macro";
import { useReducer } from "react";
import appReducer from "./appReducer";
import { gotoNextSlide, gotoPrevSlide } from "./appActions";
import { Button } from "./components/lib";
import QuestionItem from "./components/qustionItem";

function App({ json }) {
  const [state, dispatch] = useReducer(appReducer, {
    currentSlide: 1,
    answers: json.map((item) => ({ questionId: item.id, answer: "" })),
    allTimersIsOver: !json.some((item) => item.timer),
  });

  const currentQuestion = json[state.currentSlide - 1];
  const qustionsWithoutTimer = json
    .filter((item) => !item.timer)
    .map((item) => item.id);
  const showSumbitButton =
    state.answers.every((item) => item.answer) ||
    (state.allTimersIsOver &&
      state.answers
        .filter((item) => qustionsWithoutTimer.includes(item.questionId))
        .every((item) => item.answer));

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("YOUR ANSWERS", state.answers);
  };

  return (
    <form onSubmit={handleSubmit}>
      <QuestionItem
        {...currentQuestion}
        slideInfo={{
          current: state.currentSlide,
          length: state.answers.length,
        }}
        answer={state.answers[state.currentSlide - 1].answer}
        dispatch={dispatch}
      />
      <div
        css={{
          marginTop: 30,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <div css={{ marginBottom: 30 }}>
          <Button
            css={{ marginRight: 10 }}
            variant="secondary"
            type="button"
            onClick={() => dispatch(gotoPrevSlide())}
          >
            Назад
          </Button>
          <Button type="button" onClick={() => dispatch(gotoNextSlide())}>
            Вперед
          </Button>
        </div>
        {showSumbitButton && <Button type="submit">Показать результат</Button>}
      </div>
    </form>
  );
}

export default App;
