const ActionTypes = {
  NEXT_SLIDE: "NEXT_SLIDE",
  PREV_SLIDE: "PREV_SLIDE",
  SET_ANSWER: "SET_ANSWER",
  SET_ALL_TIMERS_OVER: "SET_ALL_TIMERS_OVER",
};

export default ActionTypes;
