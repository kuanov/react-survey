import ActionTypes from "./appActionTypes";

export const gotoNextSlide = () => ({
  type: ActionTypes.NEXT_SLIDE,
});

export const gotoPrevSlide = () => ({
  type: ActionTypes.PREV_SLIDE,
});

export const setAnswer = (answer) => ({
  type: ActionTypes.SET_ANSWER,
  payload: answer,
});

export const setAllTimersOver = () => ({
  type: ActionTypes.SET_ALL_TIMERS_OVER,
});
