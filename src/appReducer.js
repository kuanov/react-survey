import ActionTypes from "./appActionTypes";

const reducer = (state, action) => {
  switch (action.type) {
    case ActionTypes.NEXT_SLIDE:
      return {
        ...state,
        currentSlide:
          state.currentSlide === state.answers.length
            ? 1
            : state.currentSlide + 1,
      };
    case ActionTypes.PREV_SLIDE:
      return {
        ...state,
        currentSlide:
          state.currentSlide === 1
            ? state.answers.length
            : state.currentSlide - 1,
      };
    case ActionTypes.SET_ANSWER: {
      const { questionId, answer } = action.payload;

      return {
        ...state,
        answers: state.answers.map((item) =>
          item.questionId === questionId ? { questionId, answer } : item
        ),
      };
    }
    case ActionTypes.SET_ALL_TIMERS_OVER: {
      return {
        ...state,
        allTimersIsOver: true,
      };
    }

    default:
      return state;
  }
};

export default reducer;
