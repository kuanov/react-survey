/** @jsxRuntime classic */
/** @jsx jsx */
// eslint-disable-next-line no-unused-vars
import { jsx } from "@emotion/react/macro";
import styled from "@emotion/styled/macro";
import * as colors from "../styles/colors";

const buttonVariants = {
  primary: {
    background: colors.indigo,
    color: colors.base,
  },
  secondary: {
    background: colors.gray,
    color: colors.text,
  },
};

const Button = styled.button(
  {
    padding: "10px 15px",
    border: "0",
    fontSize: "1.1rem",
    lineHeight: "1",
    borderRadius: "3px",
  },
  ({ variant = "primary" }) => buttonVariants[variant]
);

const Textarea = styled.textarea({
  padding: "8px 12px",
  width: "100%",
  minHeight: 100,
  fontSize: "1.1rem",
  borderRadius: 5,
  background: "#f1f2f7",
  resize: "none",
});

const Fieldset = styled.fieldset({
  padding: 0,
  margin: 0,
  border: "none",
  display: "flex",
  flexDirection: "column",

  "& > legend": {
    marginBottom: 20,
    width: "100%",
  },
});

const ImageBlock = styled.div({
  marginBottom: 20,
  width: "100%",
  maxHeight: 300,
  display: "flex",
  justifyContent: "center",

  "& > img": {
    maxWidth: 300,
    width: "100%",
    height: "auto",
  },
});

export { Button, Textarea, Fieldset, ImageBlock };
