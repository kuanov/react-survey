/** @jsxRuntime classic */
/** @jsx jsx */
import { useEffect } from "react";
import { setAnswer, setAllTimersOver } from "../appActions";
import { jsx } from "@emotion/react/macro";
import { Fieldset, Textarea, ImageBlock } from "./lib";
import * as colors from "../styles/colors";
import { useTimers } from "../hooks";
import { format } from "../utils";

const QuestionItem = ({
  id,
  type,
  question,
  imgUrl,
  timer,
  options,
  slideInfo,
  answer,
  dispatch,
}) => {
  const { timers, isTimerOver, allTimersIsOver } = useTimers(timer, id);

  useEffect(() => {
    if (allTimersIsOver) {
      dispatch(setAllTimersOver());
    }
  }, [allTimersIsOver, dispatch]);

  return (
    <Fieldset disabled={isTimerOver}>
      <legend>
        <p
          css={{
            display: "flex",
            justifyContent: "space-between",
            marginBottom: 15,
          }}
        >
          <span>{`${slideInfo.current}/${slideInfo.length}`}</span>
          {timer && timers[id] && (
            <span css={{ color: colors.danger }}>
              {isTimerOver ? "Время вышло" : format(timers[id])}
            </span>
          )}
        </p>
        <p>{question}</p>
      </legend>
      {imgUrl && (
        <ImageBlock>
          <img src={imgUrl} alt="qustion img" />
        </ImageBlock>
      )}
      {type === "test" ? (
        <div
          css={{
            display: "flex",
            flexWrap: "wrap",

            "& > label": {
              width: "50%",
            },
          }}
        >
          {options.map((option) => (
            <label key={option}>
              {option}{" "}
              <input
                type="radio"
                name={question}
                value={option}
                checked={option === answer}
                onChange={(e) =>
                  dispatch(
                    setAnswer({ questionId: id, answer: e.target.value })
                  )
                }
              />
            </label>
          ))}
        </div>
      ) : (
        <Textarea
          id="answer"
          name={question}
          value={answer}
          onChange={(e) =>
            dispatch(setAnswer({ questionId: id, answer: e.target.value }))
          }
        />
      )}
    </Fieldset>
  );
};

export default QuestionItem;
