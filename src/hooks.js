import { useState, useEffect } from "react";

export const useTimers = (timer, id) => {
  const [timers, setTimers] = useState({});

  useEffect(() => {
    if (timer && !timers[id]) {
      const d = new Date(0);
      d.setSeconds(timer);
      setTimers((prev) => ({ ...prev, [id]: d }));
    }
  }, [timer, id, timers]);

  useEffect(() => {
    const timeoutIds = [];

    if (timers[id] && timers[id].getTime() > 0) {
      timeoutIds.push(
        setTimeout(() => {
          const newDate = new Date(timers[id]);
          newDate.setSeconds(newDate.getSeconds() - 1);
          setTimers((prev) => ({ ...prev, [id]: newDate }));
        }, 1000)
      );
    }

    // Object.keys(timers).forEach((key) => {
    //   if (timers[key] && timers[key].getTime() > 0) {
    //     timeoutIds.push(
    //       setTimeout(() => {
    //         const newDate = new Date(timers[key]);
    //         newDate.setSeconds(newDate.getSeconds() - 1);
    //         setTimers((prev) => ({ ...prev, [key]: newDate }));
    //       }, 1000)
    //     );
    //   }
    // });

    return () => {
      timeoutIds.forEach((t) => clearTimeout(t));
    };
  }, [timers, id]);

  return {
    timers,
    isTimerOver: timers[id]?.getTime() === 0,
    allTimersIsOver:
      Object.keys(timers).length &&
      Object.keys(timers).every((timer) => {
        return timers[timer]?.getTime() === 0;
      }),
  };
};
