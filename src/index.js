import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./index.css";
import survey from "./survey.json";

ReactDOM.render(
  <React.StrictMode>
    <App json={survey} />
  </React.StrictMode>,
  document.getElementById("root")
);
