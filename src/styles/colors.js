export const gray = "#f1f2f7";
export const indigo = "#3f51b5";
export const danger = "#ef5350";
export const base = "white";
export const text = "#434449";
