export const format = (date) => {
  const minStr = date.getMinutes().toString().padStart(2, "0");
  const secStr = date.getSeconds().toString().padStart(2, "0");

  return `${minStr} : ${secStr}`;
};
